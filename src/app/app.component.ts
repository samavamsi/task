import { Component, Inject, OnInit } from '@angular/core';
import { NotesService } from './notes.service';
import { Note } from './note';
import Notes from './notes.fixture';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  notes: Note[] = [
    {
      id: 1,
      title: 'Mock Title',
      body: 'mock body',
      color: '#ff0000',
      favourite: true
    }
  ];
  newNoteForm: Note  = { title: '' , body: '' , color: '',  favourite: false } as Note;
  selected: Note  = {...this.newNoteForm} as Note;

  constructor( private service: NotesService) {

  }

  ngOnInit() {
    this.loadNotes();
  }

  getNotes() {
    return this.notes;
  }

  private loadNotes(): void {
    // TODO: Retrieve a list of notes from the service and store them locally
    this.notes  =  this.service.getNotes();
  }

  selectNote(note) {
    // TODO: prevent changing original object
    this.selected = {...note};
  }

  newNote() {
    this.selected = {...this.newNoteForm};
  }

  saveNote(note) {
    // TODO: save note
    console.log(note);
    this.service.saveNote(note);
    this.selected = {...this.newNoteForm};
    this.loadNotes();
  }
}
